const WebSocket = require('ws')

const ws = new WebSocket('ws://localhost:8000')

ws.on('open', function open() {
  ws.send('I am IoT-Consumer 1')
})

ws.on('message', function incoming(data){
 console.log('got: ' + data)
})
