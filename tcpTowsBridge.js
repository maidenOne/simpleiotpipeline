const net = require('net');
const WebSocket = require('ws');

const server = net.createServer();

const ws = new WebSocket('ws://localhost:8000')

ws.on('open', function open() {
  ws.send('I am tcpBridge')
})

ws.on('message', function incoming(data){
 console.log('FromWS: ' + data)
})


server.on('connection', connection => {
  connection.setEncoding('utf-8');
  console.log('client connected')
  ws.send('client connected')

  connection.on('data', data => {
        console.log('client data:' + data)
        ws.send(data) // pass data to websocket server
    })

  //A close event is emmited when a connection is disconnected from the server
  connection.on('close', () => {
    ws.send('client closed')
  })

  //Handle error events
  connection.on('error', error => {
    connection.write(`Error : ${error}`);
  })

});

server.on('close', () => {
  console.log(`Server disconnected`)
});

server.on('error', error => {
  console.log(`Error : ${error}`);
});

server.listen(4000);
