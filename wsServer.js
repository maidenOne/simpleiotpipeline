const WebSocket = require('ws');
var request = require('request');

const wsServer = new WebSocket.Server({ port: 8000 });

// Broadcast to all.
wsServer.broadcast = function broadcast(data) {
  wsServer.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};

wsServer.on('connection', function connection(ws) {
  this.account_id = 0;
  ws.on('message', function incoming(data) {
    if(data != undefined){
        console.log(data);

        try
        {
           var json = JSON.parse(data);
           if(json.method){
              console.log("was json, had metod, call API server\n")
              var options = {
              uri:  'http://localhost:8888/api',
              method: 'POST',
              json: true,
              body: data
           }
              request(options,
              function (error, response, body) {
                console.log(body)
                ws.send(JSON.stringify(body))
              }
            );
          }
        }
        catch(e)
        {
           console.log('invalid json');
        }
    }

    // Broadcast to everyone else.
    wsServer.clients.forEach(function each(client) {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    });
  });
});

