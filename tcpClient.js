var net = require('net')

const args = process.argv.slice(2) // first two is cmd call and file

var client = net.connect({port : 4000},function(){
    console.log('connected to the server')
})

client.on('data',function(data){
    console.log(data.toString())
    //client.end()
})

function randomTemp(){
  return Math.floor(Math.random() * 21) - 10;
}

function randomHumidity(){
  return Math.floor(Math.random() * 101);
}

function sendData(){
  client.write("{\"id\":"+ args[0] +",\"temp\":"+ randomTemp() +",\"humidity\":" + randomHumidity() + "}")
}

setInterval(sendData, 1500);
